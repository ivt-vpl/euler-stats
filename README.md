# Euler tracking script

The folder `tracking` contains the code to track the utilization (cores and
space) on Euler. Currently, both scripts run on the `vplworker` account on
`ifalik` as cron jobs:

```
*/5 * * * * /nas/vplworker/euler/record.sh
00 00 * * * /nas/vplworker/euler/record_space.sh
```

Note that the `*.sh` scripts make use of a private key file to connect
to Euler, which is called `key`. If you want to replicate the tracking
part you also need to provide a valid private key that is accepted by
Euler (= the public key must be registered there). This means that you need
to do this through an account *other than* `vplworker`, because this account
is not known to Euler.

The folder `analysis` contains the `Analysis.ipynb` notebook which produces
the plots and downloads the data from `vplworker`. So in order to use the
notebook you need to have access to the `vplworker` account - unless you
just change all the scripts and make them run with your own account.
