import fileinput
import re
import datetime
import smtplib
from email.mime.text import MIMEText

try:
    header = None
    timestamp = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")

    for line in fileinput.input():
        row = re.split(r'\s+', line.strip())

        if header is None:
            header = row
        else:
            name = row[header.index("USER/GROUP")]
            pending = int(row[header.index("PEND")])
            running = int(row[header.index("RUN")])

            if name == "es_ivtvpl":
                continue

            if pending > 0 or running > 0:
                output = "%s %s %d %d" % (timestamp, name, pending, running)
                print(output)

except e:
    from_address = "vplworker@ethz.ch"
    to_address = "sebastian.hoerl@ivt.baug.ethz.ch"

    message = MIMEText(str(e))
    message["Subject"] = "Error in Euler usage query"
    message["From"] = from_address
    message["To"] = to_address

    smtp = smtplib.SMTP('localhost')
    smtp.sendmail(from_address, [to_address], message.as_string())
    smtp.quit()
