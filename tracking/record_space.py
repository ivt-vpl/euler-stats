import fileinput
import re
import datetime

timestamp = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")

for line in fileinput.input():
    row = re.split(r"\s+", line.strip())

    bytes = int(row[0])
    name = row[1].split("/")[-1]

    if name.startswith("lsf"):
        continue

    output = "%s %s %d" % (timestamp, name, bytes)
    print(output)
